//
//  LSMainWebViewController.swift
//  Lifestyle
//
//  Created by Матвей Малацион on 19.08.2018.
//  Copyright © 2018 Lifestyle. All rights reserved.
//

import UIKit
import MaterialComponents
import SWRevealViewController
import SwiftSoup
import JavaScriptCore
//import WKCookieWebView

class LSMainWebViewController: UIViewController, UIScrollViewDelegate, UIWebViewDelegate, LSSideMenuDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate{
    func goLogout() {
        let cstorage = HTTPCookieStorage.shared
        if let cookies = cstorage.cookies {
            for cookie in cookies {
                cstorage.deleteCookie(cookie)
            }
        }
        let cookie = HTTPCookie(properties: [
            HTTPCookiePropertyKey.name : "isMobile",
            HTTPCookiePropertyKey.value : "true",
            HTTPCookiePropertyKey.path : "/",
            HTTPCookiePropertyKey.domain : AppDelegate.cookieDomain
            ])
        
        
        
        let cookie1 = HTTPCookie(properties: [
            HTTPCookiePropertyKey.name : "isIos",
            HTTPCookiePropertyKey.value : "true",
            HTTPCookiePropertyKey.path : "/",
            HTTPCookiePropertyKey.domain : AppDelegate.cookieDomain
            ])
        
        HTTPCookieStorage.shared.setCookie(cookie!)
        HTTPCookieStorage.shared.setCookie(cookie1!)
        
        AppDelegate.saveCookies()
        AppDelegate.loadCookies()
        
        handleNavigationAction(url: self.site)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if UserDefaults.standard.bool(forKey: "onboardingShown") != true{
            let vc = OnboardingViewController(nibName: String(describing: OnboardingViewController.self), bundle: nil)
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.isModalInPopover = true
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
//    navigateTo('/profile/orders')
//    navigateTo('/profile/reviews')
//    navigateTo('/profile/reviews')
//    navigateTo('./', {popup: 'invite-friend'})
//    navigateTo('/profile/settings')
//    navigateTo('./', {popup: 'location'})
//    navigateTo('/bonus-program')
//    navigateTo('/calculator')
//    navigateTo('/help')
//    navigateTo('/company/' + id)
    
    
    @objc func goOrders() {
        let url = "'/profile/orders'"
        handleNavigationAction(url : url)
    }
    
    @objc func goReviews() {
        let url = "'/profile/reviews'"
        handleNavigationAction(url : url)
    }
    
    @objc func goInvite() {
        let url = "'', {popup: 'invite-friend'}"
        handleNavigationAction(url : url)
    }
    
    @objc func goSettings() {
        let url = "'/profile/settings'"
        handleNavigationAction(url : url)
    }
    
    @objc func goCity() {
        let url = "'', {popup: 'location'}"
        handleNavigationAction(url : url)
    }
    
    @objc func goBonus() {
        let url = "'/bonus-program'"
        handleNavigationAction(url : url)
    }
    
    @objc func goCalc() {
        let url = "'/calculator'"
        handleNavigationAction(url : url)
    }
    
    @objc func goCall() {
        let phone = "+79645243231"
        if let url = URL(string: "tel://\(phone)") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func goHelp() {
        let url = "'/help'"
        handleNavigationAction(url : url)
    }
    
    @objc func goLogin() {
        let url = "'', {popup: 'login'}"
        handleNavigationAction(url : url)
    }
    
    func handleNavigationAction(url : String){
        self.reveal.revealToggle(true)
        
        self.webView.stringByEvaluatingJavaScript(from: "window.navigateTo(\(url))")
    }
    

    @IBOutlet weak var activityIndicator: MDCActivityIndicator!
    var webView : UIWebView!
    var overlayView : UIView!
    let site = "https://bonration.ru/"
//    @IBOutlet weak var backButton: UIButton!
    var refreshControl : UIRefreshControl!
    
    var tableView : UITableView!
    
    var api : LSApi!
    
    var reveal : SWRevealViewController!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        activityIndicator.cycleColors = [UIColor(red: 54/255.0, green: 210/255.0, blue: 161/255.0, alpha: 1)]
        
        activityIndicator.startAnimating()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        
        overlayView = UIView(frame: CGRect(x: 0, y: 0, width: UIApplication.shared.statusBarFrame.width, height: UIApplication.shared.statusBarFrame.height))
        
        webView = UIWebView(frame: self.view.frame)
        let request =  URLRequest(url: URL(string: "\(site)search")!)
        webView.loadRequest(request)
        
        webView.delegate = self
        webView.scrollView.alwaysBounceVertical = false
        webView.scrollView.alwaysBounceHorizontal = false
        
        self.view.insertSubview(webView, at: 0)
        self.view.addSubview(overlayView)


        overlayView.backgroundColor = UIColor.white
        
        self.webView.scrollView.addSubview(refreshControl)
        self.webView.scrollView.delegate = self
        
        self.reveal.rearViewRevealWidth = self.view.bounds.width * 0.8
        
        tableView = UITableView()
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.shadowRadius = 1
        tableView.layer.shadowColor = UIColor.black.cgColor
        tableView.layer.shadowOffset = CGSize(width: 3, height: 3)
        tableView.layer.shadowOpacity = 0.3
        tableView.isHidden = true
        tableView.layer.borderColor = UIColor.gray.cgColor
        tableView.layer.borderWidth = 1
        self.view.addSubview(tableView)
        configureNavBar(title: nil, isRoot: nil)
        
        
        reveal.delegate = self
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggests.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = suggests[indexPath.row].name
        return cell
    }
    
    func navBarForBack(){
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "back_nav"), style: .plain, target: self, action: #selector(closeSearch))
        self.navigationItem.leftBarButtonItem = button
        self.navigationItem.title = "Подтверждение"
        self.webView.goBack()
        
    }
    
    func configureNavBar(title: String?, isRoot: Bool?){
        
        if (!webView.canGoBack || (isRoot ?? false)){
            let button = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(toggle))
            button.tintColor = .black
            self.navigationItem.leftBarButtonItem = button
        
            let searchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(search))
            button.tintColor = .black
            searchButton.tintColor = .black
            self.navigationItem.rightBarButtonItem = searchButton
            
            reveal.panGestureRecognizer().isEnabled = true && !isFilterRaised
                        
        }else{
            let button = UIBarButtonItem(image: #imageLiteral(resourceName: "back_nav"), style: .plain, target: self, action: #selector(goBack))
            button.tintColor = .black
            self.navigationItem.leftBarButtonItem = button
            
            self.navigationItem.rightBarButtonItem = nil
            
            reveal.panGestureRecognizer().isEnabled = false
        }
        
        reveal.frontViewShadowOpacity = 0.3
        
        if(title == nil){
            let image = UIImageView(image: UIImage(named: "life-style"))
            image.contentMode = .scaleAspectFit
            image.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
            
            var titleView = UIView()
            titleView.addSubview(image)
            
            self.navigationItem.titleView = titleView
            image.center = CGPoint(x: titleView.bounds.size.width / 2, y: titleView.bounds.size.height / 2)
//            self.navigationItem.titleView?
//            self.navigationItem.titleView?.bounds = CGRect(x: 0, y: 0, width: 20, height: 44)
        }else{
            self.navigationItem.titleView = nil
            self.navigationItem.title = title
        }
        
        
        reveal.panGestureRecognizer().delegate = self
        self.view.addGestureRecognizer(reveal.panGestureRecognizer())
        
        
        
    }

    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {

        return true        
    }
    

    
    @objc func goBack(){
        if(webView.request?.url?.absoluteString.contains(self.site)) ?? false{
            self.webView.stringByEvaluatingJavaScript(from: "window.goBack()")
        }else{
            webView.goBack()
        }
    }
    
    @objc func search(){
        
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = nil
        searchBar = UISearchBar()
        searchBar?.barStyle = .default
        searchBar?.showsCancelButton = false
        searchBar?.delegate = self
        searchBar?.setImage(UIImage(), for: .clear, state: .normal)
        searchBar?.backgroundImage = UIImage()
        searchBar?.layer.borderWidth = 0
        searchBar?.delegate = self
        
        self.navigationItem.titleView = searchBar
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(closeSearch))
        self.navigationItem.rightBarButtonItem?.tintColor = .black
        searchBar?.becomeFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.webView.stringByEvaluatingJavaScript(from: "window.searchCompanies('\(searchBar.text ?? "")')")
    }
    
    @objc func closeSearch(){
        self.searchBar?.resignFirstResponder()
        self.webView.stringByEvaluatingJavaScript(from: "window.searchCompanies('')")
        configureNavBar(title: nil, isRoot: true)
    }
    
    var searchBar : UISearchBar?
    
    
    var task : URLSessionDataTask?
    var suggests : [SuggestItem] = []
    
    public var topDistance : CGFloat{
        get{
            if self.navigationController != nil && !self.navigationController!.navigationBar.isTranslucent{
                return 0
            }else{
                let barHeight=self.navigationController?.navigationBar.frame.height ?? 0
                let statusBarHeight = UIApplication.shared.isStatusBarHidden ? CGFloat(0) : UIApplication.shared.statusBarFrame.height
                return barHeight + statusBarHeight
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        task?.cancel()
        
        let success : ([SuggestItem])->() = { items in
            self.suggests = items
            DispatchQueue.main.async {
                self.tableView.reloadData()
                let height : CGFloat = CGFloat(min(Float(self.suggests.count), 4.0) * 44)
                self.tableView.frame = CGRect(x: 0, y: self.topDistance, width: self.view.bounds.width, height: height)
            }
            
        }
        
        let failure : (Error?, ErrorReason) -> () = { err, reason in
            
        }
        
        task = self.api.getSuggests(q: searchText, success: success, failure: failure)
        self.webView.stringByEvaluatingJavaScript(from: "window.searchCompanies('\(searchText)')")
    }
    
    @objc func toggle(){
        reveal.revealToggle(animated: true)
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
//        self.backButton.layer.cornerRadius = 22
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let item = suggests[indexPath.row]
        self.closeSearch()
        let arg = "window.navigateTo(\("'/company/\(item.id ?? 0)'"))"
        
        self.webView.stringByEvaluatingJavaScript(from: arg)
        
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.tableView.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.tableView.isHidden = true
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        AppDelegate.saveCookies()
        if(request.url?.query ?? "").contains("iosAction"){
            sendToken()
        }
        
        return true
    }
    
    func sendToken(){
        AppDelegate.saveCookies()
        
        let success : (Any) -> () = {data in
            print("token sended")
        }
        
        let failure : (Error) -> () = {err in
            print(err)
        }
        
        self.api.postToken(success: success, failure: failure)
    }
    
    var shouldShow = false
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        for cookie in HTTPCookieStorage.shared.cookies!{
            print("\(cookie.name) \(cookie.value)")
        }
        AppDelegate.saveCookies()
        self.refreshControl.endRefreshing()
        self.activityIndicator.stopAnimating()
        
        self.handleNavBar()
        
        let context = webView.value(forKeyPath: "documentView.webView.mainFrame.javaScriptContext") as! JSContext
        
        let authBlock : @convention(block) () -> Void = {
            DispatchQueue.main.async {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                self.shouldShow = true
                self.sendToken()
                self.configureNavBar(title: nil, isRoot: true)
                print("test")
            }
            
        }
        
        let navBlock : @convention(block) (String) -> Void = { mode in
            DispatchQueue.main.async {
                
                if self.reveal.frontViewPosition == .right{
                    self.reveal.frontViewPosition = .left
                }
                
                if !(self.searchBar?.isFirstResponder ?? false){
                    if mode == "arrow"{
                        self.navigationController?.setNavigationBarHidden(false, animated: true)
                        self.shouldShow = true
                        self.configureNavBar(title: nil, isRoot: nil)
                    }else if mode == "burger"{
//                        print(webView.stringByEvaluatingJavaScript(from: "window.pageTitle"))
                        self.navigationController?.setNavigationBarHidden(false, animated: true)
                        self.shouldShow = true
                        self.configureNavBar(title: nil, isRoot: true)
                    }else if mode == "hide"{
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
                        self.shouldShow = false
                        self.configureNavBar(title: nil, isRoot: nil)
                    }
                }
                
                
            }
        }
        
        let filtersOpened : @convention(block) (String) -> Void = { mode in
            DispatchQueue.main.async {
                self.isFilterRaised = true
                self.reveal.panGestureRecognizer().isEnabled = false
                
            }
        }
        
        let filtersClosed : @convention(block) (String) -> Void = { mode in
            DispatchQueue.main.async {
                self.isFilterRaised = false
                self.reveal.panGestureRecognizer().isEnabled = true
                
            }
        }
        
        
        
        context.setObject(unsafeBitCast(filtersOpened, to: AnyObject.self), forKeyedSubscript: "filtersOpened" as NSString)
        context.setObject(unsafeBitCast(navBlock, to: AnyObject.self), forKeyedSubscript: "iosNavbarHandler" as NSString)
        context.setObject(unsafeBitCast(authBlock, to: AnyObject.self), forKeyedSubscript: "authStatusChanged" as NSString)
        context.setObject(unsafeBitCast(filtersClosed, to: AnyObject.self), forKeyedSubscript: "filtersClosed" as NSString)
        
    }
    
    var isFilterRaised = false
    
    func handleNavBar(){
        
        if !(webView.request?.url?.absoluteString ?? "").contains(self.site){
            self.configureNavBar(title: "Подтверждение", isRoot: nil)
        }
        
        if let html = webView.stringByEvaluatingJavaScript(from: "window.pageTitle"){
            if (html.count > 0){
                self.configureNavBar(title: html, isRoot: nil)
            }else{
                self.configureNavBar(title: nil, isRoot: nil)
            }
        }else{
            self.configureNavBar(title: nil, isRoot: nil)
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.configureNavBar(title: nil, isRoot: nil)
        AppDelegate.saveCookies()
        
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        AppDelegate.saveCookies()
    }
    
    
    @objc func refresh(){
        self.webView.reload()
        
    }
    
    func getTitle(){
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        overlayView.frame = CGRect(x: 0, y: 0, width: UIApplication.shared.statusBarFrame.width, height: UIApplication.shared.statusBarFrame.height)
        webView.frame = self.view.bounds;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        self.webView.goBack()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UIColor{
    static var accent : UIColor{
        get{
            return UIColor(red: 61/255.0, green: 216/255.0, blue: 171/255.0, alpha: 1)
        }
    }
}
