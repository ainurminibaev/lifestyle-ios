//
//  OnboardingViewController.swift
//  Lifestyle
//
//  Created by Матвей Малацион on 06/10/2018.
//  Copyright © 2018 Lifestyle. All rights reserved.
//

import UIKit
import MaterialComponents

class OnboardingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBAction func next(_ sender: Any) {
        self.pageControl.currentPage = self.pageControl.currentPage + 1
        self.collectionView.scrollToItem(at: IndexPath(item: self.pageControl.currentPage, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
             self.scrollViewDidEndDecelerating(self.collectionView)
        })
        
    }
    @IBOutlet weak var endButton: UIButton!
    
    @IBAction func skip(_ sender: UIButton) {
        self.finish(sender)
    }
    
    @IBOutlet weak var skip: MDCButton!
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    
    @IBAction func finish(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "onboardingShown")
        UserDefaults.standard.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib(nibName: String.init(describing: OnboardingCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String.init(describing: OnboardingCollectionViewCell.self))
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.greetingsLabel.isHidden = false
        self.endButton.isHidden = true
        
        self.endButton.layer.cornerRadius = 6
        self.endButton.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    var data = [(UIImage(named: "life-style"), "Добро пожаловать в Bon Ration!", "Агрегатор сервисов доставки\nздоровой еды. "),
                (UIImage(named: "companies"), "Выберите компанию", "Подберите компанию исходя из\nваших критериев по цене и\nкалориям."),
                (UIImage(named: "calendar-text"), "Выберите рацион", "Подберите подходящий план\nпитания."),
                (UIImage(named: "account-card-details"), "Оформите заказ", "Оформите заказ и оплатите\nудобным для вас способом."),
                (UIImage(named: "truck-delivery"), "Получите заказ", "Компания бесплатно доставит\nзаказ, а вы насладитесь здоровой\nедой и результатами."),]
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: OnboardingCollectionViewCell.self), for: indexPath) as! OnboardingCollectionViewCell
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        let item = data[indexPath.item]
        
        cell.titleLabel.text = item.1
        cell.imageView.image = item.0
        cell.backgroundColor = UIColor.clear
        
        
        let attributedString = NSMutableAttributedString(string: item.2, attributes: [
            .foregroundColor : UIColor.black.withAlphaComponent(0.54),
            .font : UIFont.systemFont(ofSize: 16, weight: .regular)
            ])
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.maximumLineHeight = 24 // Whatever line spacing you want in points
        paragraphStyle.minimumLineHeight = 24
        paragraphStyle.alignment = .center
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        cell.descriptionLabel.attributedText = attributedString
        
        if indexPath.row == 0{
            cell.innerConstraint.constant = 24
            cell.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        }else{
            cell.innerConstraint.constant = 69
            cell.titleLabel.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.view.bounds.height < 651{
            self.bottomConstraint.constant = -25
//            self.skip.setTitleFont(UIFont.systemFont(ofSize: 10), for: .)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
        
        endButton.isHidden = index != 4
        nextButton.isHidden = index == 4
//        greetingsLabel.isHidden = index != 0
        skip.isHidden = index == 4
        
        self.pageControl.currentPage = index
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.greetingsLabel.transform = CGAffineTransform.identity
        self.greetingsLabel.transform = CGAffineTransform(translationX: -self.collectionView.contentOffset.x, y: 0)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
