//
//  AppDelegate.swift
//  Lifestyle
//
//  Created by Матвей Малацион on 19.08.2018.
//  Copyright © 2018 Lifestyle. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import UserNotificationsUI
import FirebaseMessaging
import SWRevealViewController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
   
    static let cookieDomain = "bonration.ru"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let cookie = HTTPCookie(properties: [
            HTTPCookiePropertyKey.name : "isMobile",
            HTTPCookiePropertyKey.value : "true",
            HTTPCookiePropertyKey.path : "/",
            HTTPCookiePropertyKey.domain : AppDelegate.cookieDomain
            ])
        
        
        
        let cookie1 = HTTPCookie(properties: [
            HTTPCookiePropertyKey.name : "isIos",
            HTTPCookiePropertyKey.value : "true",
            HTTPCookiePropertyKey.path : "/",
            HTTPCookiePropertyKey.domain : AppDelegate.cookieDomain
            ])
        
        HTTPCookieStorage.shared.setCookie(cookie!)
        HTTPCookieStorage.shared.setCookie(cookie1!)
        
        let base = "https://api.bonration.ru/"
        let api = LSApi(base: base)
        
        let vc = LSMainWebViewController(nibName: String(describing: LSMainWebViewController.self), bundle: nil)
        let sideVc = LSSideTableViewController(style: .plain)
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.barTintColor = .white
        nc.navigationBar.isOpaque = false
        let revealVc = SWRevealViewController(rearViewController: sideVc, frontViewController: nc)
        vc.reveal = revealVc
        sideVc.delegate = vc
        vc.api = api
        sideVc.api = api
        
        self.window?.rootViewController = revealVc
        
        if #available(iOS 10.0, *) {
            let center =  UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (result, error) in
                //handle result of request failure
                print(result)
            }
            center.delegate = self
            application.registerForRemoteNotifications()
        }
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        print("----- \(InstanceID.instanceID().token())")
        
        AppDelegate.loadCookies()
        
        for var cookie in HTTPCookieStorage.shared.cookies!{
            print("\(cookie.name) \(cookie.value)")
        }
        
        return true
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: Data) {
//        self.sendDeviceTokenToServer(data: deviceToken)
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError
        error: Error) {
        // Try again later.
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        AppDelegate.saveCookies()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        AppDelegate.loadCookies()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        AppDelegate.saveCookies()
    }

    func handle(localNotification notificationItem: UNNotificationRequest){
    
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse, withCompletionHandler
        completionHandler: @escaping () -> Void) {
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    static func saveCookies() {
        guard let cookies = HTTPCookieStorage.shared.cookies else {
            return
        }
        let array = cookies.flatMap { (cookie) -> [HTTPCookiePropertyKey: Any]? in
            cookie.properties
        }
        UserDefaults.standard.set(array, forKey: "cookies")
        UserDefaults.standard.synchronize()
    }
    
    
    
    static func loadCookies() {
        guard let cookies = UserDefaults.standard.value(forKey: "cookies") as? [[HTTPCookiePropertyKey: Any]] else {
            return
        }
        cookies.forEach { (cookie) in
            guard let cookie = HTTPCookie.init(properties: cookie) else {
                return
            }
            HTTPCookieStorage.shared.setCookie(cookie)
        }
    }

}

