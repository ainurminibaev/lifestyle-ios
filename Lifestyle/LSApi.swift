//
//  LSApi.swift
//  Lifestyle
//
//  Created by Матвей Малацион on 26.08.2018.
//  Copyright © 2018 Lifestyle. All rights reserved.
//

import UIKit
import FirebaseInstanceID

struct User: Codable{
    var id : Int?
    var image : String?
    var role : String?
    var points : Int?
    var name : String?
    var phone : String?
    var email : String?
    var registration_date : Int?
    var invite_code : String?
    var reviews : Int?
}

struct SuggestItem : Codable{
    var id : Int?
    var name : String?
}

enum ErrorReason : String{
    case cookieMissing
    case otherReason
}

class LSApi: NSObject {
    
    var baseDomen : String
    
    init(base : String){
        baseDomen = base
    }
    
    func getSuggests(q: String, success: @escaping ([SuggestItem])->()?, failure: @escaping (Error?, ErrorReason)->()?) -> URLSessionDataTask{
        let url = URL(string: "\(baseDomen)v1/companies/list")!
        
        let json = [
            "companies_sort_type" : "BY_RATING",
            "limit" : 1000,
            "page" : 1,
            "query_str" : q
            ] as [String : Any]
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        if let cookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "token"}){
            urlRequest.setValue("Bearer \(cookie.value)", forHTTPHeaderField: "Authorization")
        }
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        
        var block : (Data?, URLResponse?, Error?)->() = { data, response, error in
            if error != nil{
                failure(error, .otherReason)
                return
            }
            
            do {
                guard let stringDic = try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String : Any] else{
                    failure(error, .otherReason)
                    return
                }
                
                if stringDic["error"] != nil && (stringDic["error"] as? String) != nil{
                    failure(error, .otherReason)
                }else{
                    guard let result = stringDic["result"] else{
                        failure(nil, .otherReason)
                        return
                    }
                    
                    if let suggests = try? JSONDecoder().decode([SuggestItem].self, from: JSONSerialization.data(withJSONObject: result, options: [])){
                        
                        success(suggests)
                        
                    }else{
                        failure(nil, .otherReason)
                    }
                    
                }
            }catch{
                failure(nil, .otherReason)
            }
            
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: block)
        task.resume()
        return task
    }
    
    func getBadgeValue(success: @escaping (Int)->()?, failure: @escaping (Error?, ErrorReason)->()?){
        
        guard let authCookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "token"}),
            let userIdCookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "userId"}) else {
                failure(nil, .cookieMissing)
                return
        }
        
        let url = URL(string: "\(self.baseDomen)v1/review/requests/count")!
        var urlRequest = URLRequest(url: url)
        
        urlRequest.setValue("Bearer \(authCookie.value)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
            
            if error != nil{
                failure(error, .otherReason)
                return
            }
            
            do {
                guard let stringDic = try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String : Any] else{
                    failure(error, .otherReason)
                    return
                }
            
                if let num = stringDic["result"] as? Int{
                    success(num)
                }else{
                    failure(error, .otherReason)
                }
                
            } catch let error {
                failure(error, .otherReason)
            }
            
        }).resume()
    }
    
    func getUserInfo(success: @escaping (User)->()?, failure: @escaping (Error?, ErrorReason)->()?){
        
        guard let authCookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "token"}),
            let userIdCookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "userId"}) else {
                failure(nil, .cookieMissing)
                return
        }
        
        let url = URL(string: "\(baseDomen)v1/users/\(userIdCookie.value)")!
        var urlRequest = URLRequest(url: url)
        
        urlRequest.setValue("Bearer \(authCookie.value)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
            if error != nil{
                failure(error, .otherReason)
                return
            }
            
            do {
                guard let stringDic = try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String : Any] else{
                    failure(error, .otherReason)
                    return
                }
                
                if stringDic["error"] != nil && (stringDic["error"] as? String) != nil{
                    failure(error, .otherReason)
                }else{
                    guard let result = stringDic["result"] else{
                        failure(nil, .otherReason)
                        return
                    }
                    
                    if var user = try? JSONDecoder().decode(User.self, from: JSONSerialization.data(withJSONObject: result, options: [])){
                       success(user)
                    }else{
                        failure(nil, .otherReason)
                    }
                }
                
            } catch let error {
                failure(error, .otherReason)
            }
            
        }).resume()
    }
    
    func postToken(success: @escaping (Any)->()?, failure: @escaping (Error)->()?){
        
        let url = URL(string: "\(baseDomen)v1/users/tokens")!
        
        let json = [
            "deviceId" : UIDevice.current.identifierForVendor?.uuidString ?? "unknown",
            "type" : "IOS",
            "token" : InstanceID.instanceID().token() ?? "",
            "appBuild" : Bundle.main.infoDictionary!["CFBundleVersion"] as! String,
            "appVersion" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String,
            "fcm" : true
            ] as [String : Any]
        
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        let queryItems = json.map{
            return URLQueryItem(name: "\($0)", value: "\($1)")
        }
        
        urlComponents?.queryItems = queryItems
        
        
        
        var urlRequest = URLRequest(url: urlComponents!.url!)
        urlRequest.httpMethod = "POST"
        
        if let cookie = HTTPCookieStorage.shared.cookies?.first(where: { c in c.name == "token"}){
            urlRequest.setValue("Bearer \(cookie.value)", forHTTPHeaderField: "Authorization")
        }
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
            if error != nil{
                failure(error!)
                return
            }
            
            success(data)
        }).resume()
        
    }
}

